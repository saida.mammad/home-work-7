public class Cat extends Pet implements Foul {
    public Cat() {
        this.species = Species.CAT;
    }

    public Cat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.CAT;
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я соскучился!");
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

}
