public class Mouse extends Pet{

    public Mouse() {
        species = Species.MOUSE;
    }

    public Mouse(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        species = Species.MOUSE;
    }

    @Override
    public void respond() {
        System.out.println("Иди ко мне маленький" + this.getNickname());
    }

}
