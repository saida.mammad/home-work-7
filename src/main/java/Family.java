import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children = {};
    private Pet pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
    }

    public void addChild(Human human) {
        children = Arrays.copyOf(children, children.length + 1);
        children[children.length - 1] = human;
        human.setFamily(this);
    }

    public boolean deleteChild(int index) {
        if (index > children.length - 1 || index < 0) return false;
        Human[] childrenResult = new Human[children.length - 1];
        int j = 0;
        for (int i = 0; i < children.length; i++) {
            if (i != index) {
                childrenResult[j++] = children[i];
            }
        }
        children = childrenResult;
        return true;
    }

    public void deleteChild(Human child) {
        if (children.length > 0 && Arrays.asList(children).indexOf(child) != -1) {
            Human[] result = new Human[children.length - 1];
            int j = 0;
            for (Human aChildren : children) {
                if (aChildren.hashCode() == child.hashCode()) {
                    if (aChildren.equals(child)) {
                        continue;
                    }
                }
                result[j++] = aChildren;
            }
            children = result;
        }
    }

    public int countFamily() {
        int count = 2 + children.length;
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }


    @Override
    public String toString() {
        return "Family{" + "mother=" + mother + ", father=" + father + ", children=" + Arrays.toString(children) +
                ", pet=" + (pet != null ? ("{nickname=" + pet.getNickname() + ", age=" + pet.getAge() +
                ", trickLevel=" + pet.getTrickLevel() + ", habits=" + Arrays.toString(pet.getHabits())) : null) + "}";
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

}

