public class Main {
    public static void main(String[] args) {


        Woman human = new Woman("Saida", "Salimova");
        Man human1 = new Man("Mammad", "Salimov");


        Family family = new Family(human, human1);
        String[][] humanWeekNotes = {
                {DayOfWeek.SUNDAY.name(), "Meet with friends"},
                {DayOfWeek.MONDAY.name(), "Just go sleep"}
        };

        human.setWeekNotes(humanWeekNotes);
        human1.setWeekNotes(humanWeekNotes);

        family.addChild(new Woman("Nazrin", "Salimli"));

        Dog dog1 = new Dog();
        System.out.println(dog1);

        Dog dog = new Dog("Vafle", 5, 75, new String[]{"Привет хозяин"});
        Cat cat = new Cat("Snow", 2, 80, new String[]{"Делаю что хочу"});
        Mouse mouse = new Mouse();
        Bird bird = new Bird();

        dog.respond();
        cat.respond();
        dog.foul();
        cat.foul();

        human.greetPet(dog);
        human1.greetPet(cat);
        human.makeup();
        human1.repairCar();
    }
}

